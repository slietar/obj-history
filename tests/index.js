import { suite, test } from 'uvu';
import * as assert from 'uvu/assert';

import { History } from '../index.js';


let apply = suite('apply');

apply('should create new object properties', () => {
  let root = { foo: 35 };
  let h = new History(root);

  h.apply({ bar: 42 });
  assert.equal(root, { foo: 35, bar: 42 });
  h.apply({ baz: 16 });
  assert.equal(root, { foo: 35, bar: 42, baz: 16 });
});

apply('should delete object properties', () => {
  let root = { foo: 35 };
  let h = new History(root);

  h.apply({ foo: void 0 });
  assert.equal(root, {});
});

apply.run();


let testCreateChange = suite('createChange');

testCreateChange('should create object properties', () => {
  let root = { foo: 35 };
  let h = new History(root);

  let other = {};

  let change = h.createChange((state) => {
    state.bar = 42;
  });

  assert.equal(change, { bar: 42 });


  let change2 = h.createChange((state) => {
    state.baz = other;
  });

  assert.is(change2.baz.value, other);
});

testCreateChange('should delete object properties', () => {
  let root = { foo: 35 };
  let h = new History(root);

  let change = h.createChange((state) => {
    delete state.foo;
  });

  assert.equal(change, { foo: void 0 });
});

testCreateChange.run();


let testCommit = suite('commit');

testCommit('should remove old entries when maxEntries is set', () => {
  let root = { foo: 35 };
  let h = new History(root, { maxEntries: 10 });

  for (let index = 0; index < 15; index++) {
    h.commit({ foo: 35 + index });
  }

  assert.equal(h._history.length, 10);
  assert.equal(h._historyIndex, 9);
  assert.equal(h._history[0][0], { foo: 40 });
});

testCommit.run();

