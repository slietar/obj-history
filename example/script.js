import { History } from '../index.js';


let $ = document.querySelector.bind(document);
let textarea = $('textarea');
let undoButton = $('button:nth-of-type(1)');
let redoButton = $('button:nth-of-type(2)');
let clearButton = $('button:nth-of-type(3)');
let commitButton = $('button:nth-of-type(4)');


let object = { value: '' };
let history = new History(object);

let update = () => {
  undoButton.disabled = !history.canUndo();
  redoButton.disabled = !history.canRedo();
  clearButton.disabled = !history.hasWorkingChange();
  commitButton.disabled = !history.hasWorkingChange();
};

update();

/* textarea.onchange = () => {
  history.clearWorkingChange();

  history.commit({
    value: textarea.value
  });

  update();
}; */

textarea.oninput = () => {
  history.setWorkingChange({
    value: textarea.value
  });

  update();
};


undoButton.onclick = () => {
  history.undo();
  textarea.value = object.value;
  update();
};

redoButton.onclick = () => {
  history.redo();
  textarea.value = object.value;
  update();
};

clearButton.onclick = () => {
  history.clearWorkingChange();
  textarea.value = object.value;
  update();
};

commitButton.onclick = () => {
  history.commitWorkingChange();
  update();
};

