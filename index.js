export const Symbols = {
  replace: Symbol()
};


export class History {
  constructor(target, options = {}) {
    this._history = [];
    this._historyIndex = -1;
    this._maxEntries = options.maxEntries || Infinity;
    this._target = target;
    this._workingChange = null;

    this._classes = new Map([
      [Array, {
        clone: (target) => target.map((item) => this.clone(item)),
        apply: (change, target) => {
          for (let { type, ...arrChange } of change) {
            if (type === 'splice') {
              target.splice(arrChange.index, arrChange.deleted, ...arrChange.inserted);
            } else if (type === 'set') {
              let result = this.apply(arrChange.value, target[arrChange.index]);

              if (result !== void 0) {
                target[arrChange.index] = result;
              }
            }
          }
        },
        reverse: (change, target) => {
          let reverseChange = [];

          // for (let { type, ...arrChange } of change.reverse()) {
          for (let index = 0; index < change.length; index++) {
            let { type, ...arrChange } = change[change.length - index - 1];

            if (type === 'splice') {
              reverseChange.push({
                type: 'splice',
                index: arrChange.index,
                deleted: arrChange.inserted.length,
                inserted: target.slice(arrChange.index, arrChange.index + arrChange.deleted)
              });
            } else if (type === 'set') {
              let originalIndex = arrChange.index;

              for (let subIndex = change.length - index - 2; subIndex >= 0; subIndex--) {
                let subChange = change[subIndex];

                if (subChange.type === 'splice' && subChange.index <= originalIndex) {
                  originalIndex += (subChange.deleted - subChange.inserted.length);
                }
              }

              reverseChange.push({
                type: 'set',
                index: arrChange.index,
                value: this.reverse(arrChange.value, target[/* arrChange.index */ originalIndex])
              });
            }
          }

          return reverseChange;
        },
        createProxy: (target) => {
          let change = [];
          let length = target.length;

          let proxy = new Proxy({
            splice(index, deleted, ...inserted) {
              change.push({
                type: 'splice',
                index,
                deleted,
                inserted
              });

              length += (inserted.length - deleted);
            },
            pop() {
              this.splice(length, 1);
            },
            push(...inserted) {
              this.splice(length, 0, ...inserted);
              return length;
            },
            shift() {
              if (length > 0) {
                this.splice(0, 1);
              }
            },
            unshift(...inserted) {
              this.splice(0, 0, ...inserted);
            }
          }, {
            get: (obj, prop, receiver) => {
              if (prop in obj) {
                return obj[prop];
              }

              let index = parseInt(prop);
              let [propChange, propProxy] = this.createProxy(target[index]);

              change.push({
                type: 'set',
                index,
                value: propChange
              });

              return propProxy;
            },
            set: (obj, prop, value, receiver) => {
              let index = parseInt(prop);

              change.push({
                type: 'set',
                index,
                value: {
                  [Symbols.replace]: 0,
                  value
                }
              });

              return true;
            }
          });

          return [change, proxy];
        }
      }],

      [Object, {
        clone: (target) => Object.fromEntries(Object.entries(target).map(([key, value]) => [key, this.clone(value)])),
        apply: (change, target) => {
          if (target === null) {
            return change;
          }

          for (let [key, value] of Object.entries(change)) {
            if (!(key in target)) {
              target[key] = this.apply(value, null);
            } else if (value === void 0) {
              delete target[key];
            } else {
              let result = this.apply(value, target[key]);

              if (result !== void 0) {
                target[key] = result;
              }
            }
          }
        },
        reverse: (change, target) => {
          let reverseChange = {};

          for (let [key, value] of Object.entries(change)) {
            if (!(key in target)) {
              reverseChange[key] = void 0;
            } else if (value === void 0) {
              reverseChange[key] = target[key];
            } else {
              reverseChange[key] = this.reverse(value, target[key]);
            }
          }

          return reverseChange;
        },
        createProxy: (target) => {
          let change = {};
          let proxys = {};

          let proxy = new Proxy(change, {
            deleteProperty: (obj, prop) => {
              obj[prop] = void 0;
              delete proxys[prop];

              return true;
            },
            get: (obj, prop, receiver) => {
              if (!(prop in proxy)) {
                let [propChange, propProxy] = this.createProxy(target[prop]);
                obj[prop] = propChange;
                proxys[prop] = propProxy;
              }

              return proxys[prop];
            },
            set: (obj, prop, value, receiver) => {
              obj[prop] = isPrimitive(value) ? value : { [Symbols.replace]: 0, value };

              return true;
            }
          });

          return [change, proxy];
        }
      }],

      [Set, {
        clone: (target) => new Set(Array.from(target).map((item) => this.clone(item))),
        apply: (change, target) => {
          if (change.deleted) {
            for (let item of change.deleted) {
              target.delete(item);
            }
          } else {
            target.clear();
          }

          for (let item of change.added) {
            target.add(item);
          }
        },
        reverse: (change, target) => {
          return {
            added: new Set(change.deleted),
            deleted: new Set(change.added)
          };
        },
        createProxy: (target) => {
          let change = { added: new Set(), deleted: new Set() };

          let proxy = {
            add: (item) => {
              if (!target.has(item)) {
                change.added.add(item);
              }

              if (change.deleted) {
                change.deleted.delete(item);
              }
            },
            delete: (item) => {
              change.added.delete(item);

              if (change.deleted && target.has(item)) {
                change.deleted.add(item);
              }
            },
            clear: (item) => {
              change.added.clear();
              change.deleted = null;
            },

            has: (item) => {
              return (target.has(item) && change.deleted && !change.deleted.has(item))
                || change.added.has(item);
            },
            get size() {
              return (change.deleted ? (target.size - change.deleted.size) : 0) + change.added.size;
            }
          };

          return [change, proxy];
        }
      }]
    ]);
  }

  clone(target) {
    if (target === null) {
      return target;
    }

    if (typeof target === 'object' && Symbols.replace in target) {
      return {
        [Symbols.replace]: 0,
        value: this.clone(target.value)
      };
    }

    let methods = this._classes.get(target.constructor);

    if (methods) {
      return methods.clone(target);
    }

    return target;
  }

  // TODO: update this._target if returned value and not target
  apply(change, target = this._target) {
    if (change === null) {
      return null;
    }

    if (typeof change === 'object' && Symbols.replace in change) {
      return change.value;
    }

    let methods = this._classes.get((target !== null ? target : change).constructor);

    if (methods) {
      return methods.apply(change, target);
    } else {
      return change;
    }
  }

  reverse(change, target = this._target) {
    if (target === null) {
      return null;
    }

    if (typeof change === 'object' && Symbols.replace in change) {
      return {
        [Symbols.replace]: 0,
        value: target
      };
    }

    let methods = this._classes.get(/* (target !== null ? target : change) */ target.constructor);

    if (methods) {
      return methods.reverse(change, target);
    } else {
      return target;
    }
  }

  register(constructor, methods) {
    this._classes.set(constructor, methods);
  }


  clear() {
    this._history.length = 0;
    this._historyIndex = -1;
  }

  clearFuture() {
    this._history.length = this._historyIndex + 1;
  }

  clearPast() {
    this._history.splice(0, this._historyIndex + 1);
    this._historyIndex = -1;
  }


  commit(change, reverseChange) {
    this.commitApplied(change, reverseChange);
    this.apply(change);
  }

  commitApplied(change, reverseChange = this.reverse(change)) {
    this.clearFuture();
    this._history.push([change, reverseChange]);

    if (this._historyIndex < this._maxEntries - 1) {
      this._historyIndex++;
    } else {
      this._history.shift();
    }
  }


  canUndo() {
    return this._historyIndex >= 0;
  }

  undo() {
    if (!this.canUndo()) {
      return;
    }

    let reverseChange = this._history[this._historyIndex][1];

    this.apply(reverseChange);
    this._historyIndex--;

    return reverseChange;
  }


  canRedo() {
    return this._historyIndex < this._history.length - 1;
  }

  redo() {
    if (!this.canRedo()) {
      return;
    }

    this._historyIndex++;

    let change = this._history[this._historyIndex][0];
    this.apply(change);

    return change;
  }


  createChange(handler) {
    let [change, proxy] = this.createProxy(this._target);

    handler(proxy);
    return change;
  }

  createProxy(target) {
    let methods = this._classes.get(target.constructor);
    return methods.createProxy(target);
  }


  setWorkingChange(change) {
    if (this._workingChange) {
      this.clearWorkingChange();
    }

    if (typeof change === 'function') {
      change = this.createChange(change);
    }

    this._workingChange = [change, this.reverse(change)];
    this.apply(change);
  }

  updateWorkingChange(handler) {
    let newChange = this.clone(this._workingChange[0]);

    if (handler(newChange)) {
      this.setWorkingChange(newChange);
    } else {
      this.clearWorkingChange();
    }
  }

  hasWorkingChange() {
    return this._workingChange !== null;
  }

  clearWorkingChange() {
    if (this._workingChange) {
      let [change, reverseChange] = this._workingChange;
      this._workingChange = null;

      this.apply(reverseChange);
    }
  }

  commitWorkingChange() {
    if (this._workingChange) {
      let [change, reverseChange] = this._workingChange;
      this._workingChange = null;

      this.commitApplied(change, reverseChange);
    }
  }
}


function isPrimitive(target) {
  return typeof target !== 'object';
}

